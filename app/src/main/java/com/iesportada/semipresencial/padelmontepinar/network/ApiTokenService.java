package com.iesportada.semipresencial.padelmontepinar.network;

import com.iesportada.semipresencial.padelmontepinar.model.AddResponse;
import com.iesportada.semipresencial.padelmontepinar.model.Booking;
import com.iesportada.semipresencial.padelmontepinar.model.DeleteResponse;
import com.iesportada.semipresencial.padelmontepinar.model.Email;
import com.iesportada.semipresencial.padelmontepinar.model.EmailResponse;
import com.iesportada.semipresencial.padelmontepinar.model.GetBookingsResponse;
import com.iesportada.semipresencial.padelmontepinar.model.LogoutResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiTokenService {

    @POST("api/logout")
    Call<LogoutResponse> logout(
            //@Header("Authorization") String token
    );

    @GET("api/bookings")
    Call<GetBookingsResponse> getBookings(
            //@Header("Authorization") String token
    );

    @POST("api/bookings")
    Call<AddResponse> createBooking(
            //@Header("Authorization") String token,
            @Body Booking booking);

    @PUT("api/bookings/{id}")
    Call<AddResponse> updateBooking(
            //@Header("Authorization") String token,
            @Body Booking booking,
            @Path("id") int id);

    @DELETE("api/bookings/{id}")
    Call<DeleteResponse> deleteTask(
            //@Header("Authorization") String token,
            @Path("id") int id);

    @POST("api/email")
    Call<EmailResponse> sendEmail(@Body Email email);
}


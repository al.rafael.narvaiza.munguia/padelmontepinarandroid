package com.iesportada.semipresencial.padelmontepinar.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.Toast;

import com.iesportada.semipresencial.padelmontepinar.MainActivity;
import com.iesportada.semipresencial.padelmontepinar.R;
import com.iesportada.semipresencial.padelmontepinar.adapter.BookingAdapter;
import com.iesportada.semipresencial.padelmontepinar.adapter.ClickListener;
import com.iesportada.semipresencial.padelmontepinar.adapter.RecyclerTouchListener;
import com.iesportada.semipresencial.padelmontepinar.databinding.ActivityPanelBinding;
import com.iesportada.semipresencial.padelmontepinar.model.Booking;
import com.iesportada.semipresencial.padelmontepinar.model.DeleteResponse;
import com.iesportada.semipresencial.padelmontepinar.model.GetBookingsResponse;
import com.iesportada.semipresencial.padelmontepinar.model.LogoutResponse;
import com.iesportada.semipresencial.padelmontepinar.network.ApiTokenRestClient;
import com.iesportada.semipresencial.padelmontepinar.util.SharedPreferencesManager;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PanelActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int ADD_CODE = 100;
    public static final int UPDATE_CODE = 200;
    public static final int OK = 1;

    int positionClicked;
    ProgressDialog progreso;
    SharedPreferencesManager preferences;
    private ActivityPanelBinding binding;
    private BookingAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPanelBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.floatingActionButton.setOnClickListener(this);
        preferences = new SharedPreferencesManager(this);
        adapter = new BookingAdapter();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, binding.recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                modify(adapter.getAt(position));
                positionClicked = position;
            }

            @Override
            public void onLongClick(View view, int position) {
                confirm(adapter.getAt(position).getId(), adapter.getAt(position).toString(), position);
            }
        }));

        ApiTokenRestClient.deleteInstance();
        downloadBookings();

    }
    @Override
    public void onResume() {
        super.onResume();
        downloadBookings();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //Pide al server descargar de nuevo las reservas.
            case R.id.refresh:
                downloadBookings();
                break;
            case R.id.email:
                Intent i = new Intent(this, EmailActivity.class);
                startActivity(i);
                break;
            case R.id.exit:
                //petición al servidor para anular el token (a la ruta /api/logout)
                Call<LogoutResponse> call = ApiTokenRestClient.getInstance(preferences.getToken()).logout();
                call.enqueue(new Callback<LogoutResponse>() {
                    @Override
                    public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                        if (response.isSuccessful()) {
                            LogoutResponse logoutResponse = response.body();
                            if (logoutResponse.getSuccess()) {
                                showMessage("Logout OK");
                            } else
                                showMessage("Error in logout");
                        } else {
                            StringBuilder message = new StringBuilder();
                            message.append("Download error: " + response.code());
                            if (response.body() != null)
                                message.append("\n" + response.body());
                            if (response.errorBody() != null)
                                try {
                                    message.append("\n" + response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            showMessage(message.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<LogoutResponse> call, Throwable t) {
                        String message = "Failure in the communication\n";
                        if (t != null)
                            message += t.getMessage();
                        showMessage(message);
                    }
                });
                preferences.saveToken(null, null);
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                break;

        }
        return true;
    }

    @Override
    public void onClick(View v) {

        //TODO añadir al addactivity un datepicker y parsear su paso a string con método

        if (v == binding.floatingActionButton) {
            Intent i = new Intent(this, AddActivity.class);
            startActivityForResult(i, ADD_CODE);
        }
    }

    private void downloadBookings() {
        Context context;
        context = this;
        progreso = new ProgressDialog(context);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Descargando reservas, espere");
        progreso.setCancelable(true);
        progreso.show();

        Call<GetBookingsResponse> call = ApiTokenRestClient.getInstance(preferences.getToken()).getBookings();
        call.enqueue(new Callback<GetBookingsResponse>() {
            @Override
            public void onResponse(Call<GetBookingsResponse> call, Response<GetBookingsResponse> response) {
                progreso.dismiss();
                if (response.isSuccessful()) {
                    GetBookingsResponse getBookingsResponse = response.body();
                    if (getBookingsResponse.getSuccess()) {
                        adapter.setBookings(getBookingsResponse.getData());
                        showMessage("Reservas de la pista descargadas");
                    } else {
                        showMessage("Error descargando reservas: " + getBookingsResponse.getMessage());
                    }
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Download error: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                }
            }

            @Override
            public void onFailure(Call<GetBookingsResponse> call, Throwable t) {
                progreso.dismiss();
                String message = "Failure in the communication\n";
                if (t != null)
                    message += t.getMessage();
                showMessage(message);
            }
        });
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Booking booking = new Booking();

        if (requestCode == ADD_CODE)
            if (resultCode == OK) {
                booking.setId(data.getIntExtra("id", 1));
                booking.setStart_time(data.getStringExtra("start_time"));
                booking.setEnd_time(data.getStringExtra("end_time"));
                booking.setCreatedAt(data.getStringExtra("createdAt"));
                adapter.add(booking);
            }
        if (requestCode == UPDATE_CODE)
            if (resultCode == OK) {
                booking.setId(data.getIntExtra("id", 1));
                booking.setStart_time("start_time");
                booking.setEnd_time(data.getStringExtra("end_time"));
                booking.setUpdatedAt(data.getStringExtra("updatedAt"));
                adapter.modifyAt(booking, positionClicked);
            }
    }

    private void modify(Booking booking) {
        Intent i = new Intent(this, UpdateActivity.class);
        i.putExtra("booking", booking);
        startActivityForResult(i, UPDATE_CODE);
    }

    private void confirm(final int idBooking, String description, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(description + "\nDo you want to delete?")
                .setTitle("Delete")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        connection(position);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    private void connection(final int position) {
        Call<DeleteResponse> call = ApiTokenRestClient.getInstance(preferences.getToken()).deleteTask(adapter.getId(position));
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();
        call.enqueue(new Callback<DeleteResponse>() {
            @Override
            public void onResponse(Call<DeleteResponse> call, Response<DeleteResponse> response) {
                progreso.dismiss();
                if (response.isSuccessful()) {
                    DeleteResponse deleteResponse = response.body();
                    if (deleteResponse.getSuccess()) {
                        adapter.removeAt(position);
                        showMessage("Se eliminó la reserva");
                    } else {
                        showMessage("No se pudo eliminar la reserva");
                    }
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Error deleting a site: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                }
            }

            @Override
            public void onFailure(Call<DeleteResponse> call, Throwable t) {
                progreso.dismiss();
                if (t != null)
                    showMessage("Failure in the communication\n" + t.getMessage());
            }
        });
    }
}
package com.iesportada.semipresencial.padelmontepinar.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import com.iesportada.semipresencial.padelmontepinar.databinding.ItemViewBinding;
import com.iesportada.semipresencial.padelmontepinar.model.Booking;

import java.util.ArrayList;

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.MyViewHolder> {
    private ArrayList<Booking> bookings;

    public BookingAdapter(){this.bookings=new ArrayList<>();}

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private ItemViewBinding binding;
        public MyViewHolder(ItemViewBinding b){
            super(b.getRoot());
            binding = b;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        return new MyViewHolder(ItemViewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        Booking booking = bookings.get(position);

        holder.binding.textViewReserva.setText(booking.toString());
        //holder.binding.textViewCreatedAt.setText(booking.getCreatedAt());
    }

    @Override
    public int getItemCount(){
       return bookings.size();
    }
    public int getId(int position){
        return this.bookings.get(position).getId();
    }

    public void setBookings(ArrayList<Booking> bookings){
        this.bookings=bookings;
        notifyDataSetChanged();
    }

    public Booking getAt(int position){
        Booking booking;
        booking = this.bookings.get(position);
        return booking;
    }

    public void add(Booking booking){
        this.bookings.add(booking);
        notifyItemInserted(bookings.size() - 1);
        notifyItemRangeChanged(0, bookings.size() - 1);
    }

    public void modifyAt(Booking booking, int position){
        this.bookings.set(position, booking);
        notifyItemChanged(position);
    }

    public void removeAt(int position){
        this.bookings.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, bookings.size() - 1);
    }


}

package com.iesportada.semipresencial.padelmontepinar.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.iesportada.semipresencial.padelmontepinar.databinding.ActivityUpdateBinding;
import com.iesportada.semipresencial.padelmontepinar.model.AddResponse;
import com.iesportada.semipresencial.padelmontepinar.model.Booking;
import com.iesportada.semipresencial.padelmontepinar.network.ApiTokenRestClient;
import com.iesportada.semipresencial.padelmontepinar.util.SharedPreferencesManager;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener, Callback<AddResponse> {

    public static final int OK = 1;

    private ProgressDialog progreso;
    SharedPreferencesManager preferences;
    private ActivityUpdateBinding binding;
    private Booking booking;
    private String selectedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityUpdateBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.accept.setOnClickListener(this);
        binding.cancel.setOnClickListener(this);
        binding.editTextStartDate.setOnClickListener(this);
        binding.editTextEndDate.setOnClickListener(this);

        preferences = new SharedPreferencesManager(this);

        Intent i = getIntent();
        booking = (Booking) i.getSerializableExtra("booking");
        binding.textViewID.setText(String.valueOf(booking.getId()));
        binding.editTextStartDate.setText(String.valueOf(booking.getStart_time()));
        binding.editTextEndDate.setText(String.valueOf(booking.getEnd_time()));
    }

    @Override
    public void onClick(View v) {
        String start_date, end_date;
        hideSoftKeyboard();
        if (v == binding.accept) {
            start_date = binding.editTextStartDate.getText().toString();
            end_date = binding.editTextEndDate.getText().toString();
            if (start_date.isEmpty() || end_date.isEmpty()) {
                showMessage("Por favor, rellene los campos de la reserva");
            } else {
                booking.setStart_time(start_date);
                booking.setEnd_time(end_date);
                connection(booking);
            }
        }
        if (v == binding.editTextStartDate) {
            showTimePickerDialogFor(binding.editTextStartDate);
            showDatePickerDialogFor();
        }
        if (v == binding.editTextEndDate) {
            showTimePickerDialogFor(binding.editTextEndDate);
            showDatePickerDialogFor();
        }
        if (v == binding.cancel) {
            finish();
        }
    }

    private void connection(Booking booking) {
        showMessage(booking.getId() + "");
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        Call<AddResponse> call = ApiTokenRestClient.getInstance(preferences.getToken()).updateBooking(booking, booking.getId());
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<AddResponse> call, Response<AddResponse> response) {
        progreso.dismiss();
        if (response.isSuccessful()) {
            AddResponse addResponse = response.body();
            if (addResponse.getSuccess()) {
                Intent i = new Intent();
                Bundle bundle = new Bundle();
                bundle.putInt("id", addResponse.getData().getId());
                bundle.putString("start_time", addResponse.getData().getStartDate());
                bundle.putString("end_time", addResponse.getData().getEndDate());
                bundle.putString("createdAt", addResponse.getData().getCreatedAt());
                i.putExtras(bundle);
                setResult(OK, i);
                finish();
                showMessage("Reserva actualizada " + addResponse.getData().getId());
            } else {
                String message = "Error actualizando la reserva";
                if (!addResponse.getMessage().isEmpty()) {
                    message += ": " + addResponse.getMessage();
                }
                showMessage(message);
            }
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Download error: ");
            Log.e("Error:", response.errorBody().toString());
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<AddResponse>call, Throwable t){
        progreso.dismiss();
        if(t!= null){
            showMessage("Error en la comunicación con el servidor\n" + t.getMessage());
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void showDatePickerDialogFor() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because January is zero
                String formattedMonth;
                String formattedDay;
                month = month+1;
                if(month<11){
                    formattedMonth = "0" + month;
                }
                else {
                    formattedMonth = String.valueOf(month);
                }

                if(day<10){
                    formattedDay = "0" + day;
                }
                else{
                    formattedDay = String.valueOf(day);
                }
                setSelectedDate(year + "-" + (formattedMonth) + "-" + formattedDay);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void showTimePickerDialogFor(EditText editText){
        TimePickerFragment newFragment = TimePickerFragment.newInstance(new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker timepicker, int hour, int minutes){
                String formattedMinutes;
                String formattedHours;
                if(minutes<10){
                    formattedMinutes = "0" + minutes;
                }
                else{
                    formattedMinutes = String.valueOf(minutes);
                }
                if(hour<10){
                    formattedHours = "0" + hour;
                }
                else{
                    formattedHours = String.valueOf(hour);
                }
                final String selectedTime = formattedHours + ":" + formattedMinutes + ":" + "00";
                final String fullDateAndTime = getSelectedDate() + " " + selectedTime;
                editText.setText(fullDateAndTime);
            }

        });
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public String getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(String selectedDate) {
        this.selectedDate = selectedDate;
    }
}
package com.iesportada.semipresencial.padelmontepinar.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.iesportada.semipresencial.padelmontepinar.databinding.ActivityAddBinding;
import com.iesportada.semipresencial.padelmontepinar.model.AddResponse;
import com.iesportada.semipresencial.padelmontepinar.model.Booking;
import com.iesportada.semipresencial.padelmontepinar.network.ApiTokenRestClient;
import com.iesportada.semipresencial.padelmontepinar.util.SharedPreferencesManager;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActivity extends AppCompatActivity implements View.OnClickListener, Callback<AddResponse> {

    public static final int OK = 1;


    ProgressDialog progreso;
    SharedPreferencesManager preferences;
    private String selectedDate;

    private ActivityAddBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAddBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.accept.setOnClickListener(this);
        binding.cancel.setOnClickListener(this);
        binding.editTextStartDate.setOnClickListener(this);
        binding.editTextEndDate.setOnClickListener(this);

        preferences = new SharedPreferencesManager(this);
    }

    @Override
    public void onClick(View v) {
        String start_time;
        String end_time;
        Booking booking;

        if (v == binding.accept) {
            hideSoftKeyboard();
            start_time = binding.editTextStartDate.getText().toString();
            end_time = binding.editTextEndDate.getText().toString();
            if (start_time.isEmpty() || end_time.isEmpty()) {
                showMessage("Todas las fechas han de estar rellenadas");
            } else {
                booking = new Booking(start_time, end_time);
                connection(booking);
            }
        }
        if (v == binding.editTextStartDate) {
            showTimePickerDialogFor(binding.editTextStartDate);
            showDatePickerDialogFor();

        }
        if (v == binding.editTextEndDate) {
            showTimePickerDialogFor(binding.editTextEndDate);
            showDatePickerDialogFor();

        }
        if (v == binding.cancel) {
            finish();
        }
    }

    private void connection(Booking booking) {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        Call<AddResponse> call = ApiTokenRestClient.getInstance(preferences.getToken()).createBooking(booking);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<AddResponse> call, Response<AddResponse> response) {
        progreso.dismiss();
        if (response.isSuccessful()) {
            AddResponse addResponse = response.body();
            if (addResponse.getSuccess()) {
                Intent i = new Intent();
                Bundle bundle = new Bundle();
                bundle.putInt("id", addResponse.getData().getId());
                bundle.putString("start_time", addResponse.getData().getStartDate());
                bundle.putString("end_time", addResponse.getData().getEndDate());
                bundle.putString("createdAt", addResponse.getData().getCreatedAt());
                i.putExtras(bundle);
                setResult(OK, i);
                finish();
                showMessage("Reserva creada");
            } else {
                String message = "Error creando la reserva";
                if (!addResponse.getMessage().isEmpty()) {
                    message += ": " + addResponse.getMessage();
                }
                showMessage(message);
            }
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Error en descarga: ");
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<AddResponse> call, Throwable t) {
        progreso.dismiss();
        if (t != null)
            showMessage("Error en la comunicación\n" + t.getMessage());
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void showDatePickerDialogFor() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because January is zero
                String formattedMonth;
                String formattedDay;
                month = month+1;
                if(month<11){
                    formattedMonth = "0" + month;
                }
                else {
                    formattedMonth = String.valueOf(month);
                }

                if(day<10){
                    formattedDay = "0" + day;
                }
                else{
                    formattedDay = String.valueOf(day);
                }
                setSelectedDate(year + "-" + (formattedMonth) + "-" + formattedDay);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void showTimePickerDialogFor(EditText editText){
        TimePickerFragment newFragment = TimePickerFragment.newInstance(new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker timepicker, int hour, int minutes){
                String formattedMinutes;
                String formattedHours;
                if(minutes<10){
                    formattedMinutes = "0" + minutes;
                }
                else{
                    formattedMinutes = String.valueOf(minutes);
                }
                if(hour<10){
                    formattedHours = "0" + hour;
                }
                else{
                    formattedHours = String.valueOf(hour);
                }
                final String selectedTime = formattedHours + ":" + formattedMinutes + ":" + "00";
                final String fullDateAndTime = getSelectedDate() + " " + selectedTime;
                editText.setText(fullDateAndTime);
            }

        });
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public String getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(String selectedDate) {
        this.selectedDate = selectedDate;
    }
}